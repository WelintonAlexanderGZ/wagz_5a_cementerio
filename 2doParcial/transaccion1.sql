---------------- Transaccion tramite estructura --------------------
create or replace procedure tran_Tramite_Estructura(int,int,int,int,int,int, date, date, text)
as
$$
declare
    estado_est int;
    begin
        insert into tramite values($1,$2,$3,$4,$5,$6,$7,$8,$9);
		select id_est_estruc into estado_est from estructura where id_estruc = $5;
        if (estado_est != 1) then
            raise exception 'La estructura esta ocupada/ mantenimiento / o en construccion.';
			rollback;
		else
			raise notice 'Ingreso correcto.';
			update estructura set id_est_estruc = 2 where id_estruc = $5;
			rollback;
	end if;
    commit;
end;
$$
language plpgsql;