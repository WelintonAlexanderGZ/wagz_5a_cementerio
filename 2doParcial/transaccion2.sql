---------------- Transaccion tran_PAGO_CAUSMUERTE --------------------
create or replace procedure tran_PAGO_CAUSMUERTE(int,int,int,numeric, date)
as
$$
declare
    id_cm int;
	id_d int;
    begin
        insert into pago values($1,$2,$3,$4,$5);
		select id_difu into id_d from tramite where id_tram = $3;
		select id_cau_mue into id_cm from difunto where id_difu = id_d;
        if (id_cm = 2 and $2 !=1) then
            raise exception 'El pago por esta causa de muerte solo se puede dar por deposito bancario';
			rollback;
		else
			raise notice 'Transaccion exitosa.';
			update tramite set sms_tram = 'Este trámite podra ser pagado despues de 15 dias por medidas preventivas.' where id_tram = $3;
	end if;
    commit;
end;
$$
language plpgsql;