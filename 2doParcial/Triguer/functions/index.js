const functions = require("firebase-functions");

//Permisos de admin
const admin = require("firebase-admin");

//Iniciar como admin
admin.initializeApp();

const db = admin.firestore();

//Triguer
exports.onUserCreate = functions.firestore
    .document("tramite/{id_auto}")
    .onCreate(async (snap, context) => {
        const valor = snap.data();

        let query = db
            .collection("tramite")
            .where("fch_cumpli_tram", "==", valor.fch_cumpli_tram)
            .where("id_per", "==", valor.id_per);

        await query.get().then((snap) => {
            size = snap.size;
        });

        if (size > 3) {
            console.log(
                "El empleado llego al maximo de tramites posibles en esa fecha."
            );
            await db.collection("tramite").doc(snap.id).delete();
        }
    });
