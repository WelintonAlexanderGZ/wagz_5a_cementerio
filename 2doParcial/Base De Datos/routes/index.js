const e = require("express");
var express = require("express");
const { continueSession } = require("pg/lib/sasl");
var router = express.Router();
// Invocar al modulo de conexion de la BD
const pool = require("../config/db");

//HOME
router.get("/", function (req, res) {
  res.render("index");
});

//CLIENTE
router.get("/cliente", function (req, res) {
  pool.query("SELECT * FROM cliente", function (err, datos) {
    if (err) {
      console.log(err.stack);
    } else {
      res.render("clien/cliente", { cliente: datos.rows });
    }
  });
});

router.get("/cliente/crear", function (req, res) {
  pool.query("SELECT * FROM cliente", function (err, datos) {
    if (err) {
      console.log(err.stack);
    } else {
      const x = datos.rows.length;
      res.render("clien/crear", { ultimoid: datos.rows[x - 1].id_clien + 1 });
    }
  });
});

router.get("/cliente/editar/:id_clien", function (req, res) {
  try {
    pool.query(
      "SELECT * FROM cliente WHERE id_clien = $1",
      [req.params.id_clien],
      (err, datos) => {
        if (err) {
          throw err;
        }
        res.render("clien/editar", { cliente: datos.rows[0] });
      }
    );
  } catch (error) {
    console.log(error);
  }
});

router.post("/cliente/crear", function (req, res) {
  try {
    const query = {
      text: "INSERT INTO cliente(id_clien, id_ubi, ci_clien, name_clien, lname_clien, fch_naci_clien, tel1_clien, tel2_clien)VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
      values: [
        req.body.id,
        req.body.ubi,
        req.body.ci,
        req.body.name,
        req.body.lname,
        req.body.fch,
        req.body.tele1,
        req.body.tele2,
      ],
    };
    pool.query(query, function (err) {
      if (err) {
        console.log(err.stack);
      } else {
        res.redirect("/cliente");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

router.post("/cliente/eliminar/:id_clien", function (req, res) {
  console.log("Recepcion de datos");
  console.log(req.params.id_clien);
  try {
    const query = {
      text: "DELETE FROM cliente WHERE id_clien = $1",
      values: [req.params.id_clien],
    };
    pool.query(query, function (err) {
      if (err) {
        console.log(err.stack);
      } else {
        res.redirect("/cliente");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

router.post("/cliente/actualizar", function (req, res) {
  console.log(req.body.fch);
  try {
    if (req.body.fchn) {
      fch = req.body.fchn;
      pool.query(
        "UPDATE cliente SET id_ubi = $1, ci_clien = $2, name_clien = $3, lname_clien = $4, fch_naci_clien = $5, tel1_clien = $6, tel2_clien = $7  WHERE id_clien = $8",
        [
          req.body.ubi,
          req.body.ci,
          req.body.name,
          req.body.lname,
          req.body.fchn,
          req.body.tele1,
          req.body.tele2,
          req.body.id,
        ],
        (err, datos) => {
          if (err) {
            throw err;
          }
          console.log("Datos actualizados correctamente");
          res.redirect("/cliente");
        }
      );
    } else {
      pool.query(
        "UPDATE cliente SET id_ubi = $1, ci_clien = $2, name_clien = $3, lname_clien = $4, tel1_clien = $5, tel2_clien = $6  WHERE id_clien = $7",
        [
          req.body.ubi,
          req.body.ci,
          req.body.name,
          req.body.lname,
          req.body.tele1,
          req.body.tele2,
          req.body.id,
        ],
        (err, datos) => {
          if (err) {
            throw err;
          }
          console.log("Datos actualizados correctamente");
          res.redirect("/cliente");
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
});

//PERSONAL
router.get("/personal", function (req, res) {
  pool.query("SELECT * FROM personal", function (err, datos) {
    if (err) {
      console.log(err.stack);
    } else {
      res.render("perso/personal", { personal: datos.rows });
    }
  });
});

router.get("/personal/crear", function (req, res) {
  pool.query("SELECT * FROM personal", function (err, datos) {
    if (err) {
      console.log(err.stack);
    } else {
      const x = datos.rows.length;
      res.render("perso/crear", { ultimoid: datos.rows[x - 1].id_per + 1 });
    }
  });
});

router.get("/personal/editar/:id_per", function (req, res) {
  try {
    pool.query(
      "SELECT * FROM personal WHERE id_per = $1",
      [req.params.id_per],
      (err, datos) => {
        if (err) {
          throw err;
        }
        res.render("perso/editar", { personal: datos.rows[0] });
      }
    );
  } catch (error) {
    console.log(error);
  }
});

router.post("/personal/crear", function (req, res) {
  try {
    const query = {
      text: "INSERT INTO personal(id_per, id_tipo_per, ci_per, name_per, lname_per, fch_naci_per, tel1_per, tel2_per)VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
      values: [
        req.body.id,
        req.body.tipo,
        req.body.ci,
        req.body.name,
        req.body.lname,
        req.body.fch,
        req.body.tele1,
        req.body.tele2,
      ],
    };
    pool.query(query, function (err) {
      if (err) {
        console.log(err.stack);
      } else {
        res.redirect("/personal");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

router.post("/personal/eliminar/:id_per", function (req, res) {
  console.log("Recepcion de datos");
  console.log(req.params.id_per);
  try {
    const query = {
      text: "DELETE FROM personal WHERE id_per = $1",
      values: [req.params.id_per],
    };
    pool.query(query, function (err) {
      if (err) {
        console.log(err.stack);
      } else {
        res.redirect("/personal");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

router.post("/personal/actualizar", function (req, res) {
  console.log(req.body.fch);
  try {
    if (req.body.fchn) {
      fch = req.body.fchn;
      pool.query(
        "UPDATE personal SET id_tipo_per = $1, ci_per = $2, name_per = $3, lname_per = $4, fch_naci_per = $5, tel1_per = $6, tel2_per = $7  WHERE id_per = $8",
        [
          req.body.tipo,
          req.body.ci,
          req.body.name,
          req.body.lname,
          req.body.fchn,
          req.body.tele1,
          req.body.tele2,
          req.body.id,
        ],
        (err, datos) => {
          if (err) {
            throw err;
          }
          console.log("Datos actualizados correctamente");
          res.redirect("/personal");
        }
      );
    } else {
      pool.query(
        "UPDATE personal SET id_tipo_per = $1, ci_per = $2, name_per = $3, lname_per = $4, tel1_per = $5, tel2_per = $6  WHERE id_per = $7",
        [
          req.body.tipo,
          req.body.ci,
          req.body.name,
          req.body.lname,
          req.body.tele1,
          req.body.tele2,
          req.body.id,
        ],
        (err, datos) => {
          if (err) {
            throw err;
          }
          console.log("Datos actualizados correctamente");
          res.redirect("/personal");
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
});

//TRANSACCION 1
router.get("/transaccion1", function (req, res) {
  pool.query("SELECT * FROM tramite", function (err, dat) {
    if (err) {
      console.log(err.stack);
    } else {
      const x = dat.rows.length;
      res.render("transaccion/tran1", {
        ultimoid: x + 1,
        datos: "",
      });
    }
  });
});

router.post("/transaccion1/crear", function (req, res) {
  pool.query("SELECT * FROM tramite", function (err, dat) {
    if (err) {
      console.log(err.stack);
    } else {
      const y = dat.rows.length;
      (async () => {
        const client = await pool.connect();
        try {
          datosForm = req.body;
          console.log(datosForm);
          const cs =
            "call tran_tramite_estructura($1, $2, $3, $4, $5, $6, $7, $8, $9)";
          const csValues = [
            datosForm.id_tram,
            datosForm.id_tipo_tram,
            datosForm.id_clien,
            datosForm.id_difu,
            datosForm.id_estruc,
            datosForm.id_per,
            datosForm.fch_tram,
            datosForm.fch_cumpli_tram,
            datosForm.sms_tram,
          ];
          await client.query(cs, csValues);
          x = "Transaccion exitosa";
          res.render("transaccion/tran1", {
            ultimoid: y + 2,
            datos: x,
          });
        } catch (e) {
          x = e;
          res.render("transaccion/tran1", {
            ultimoid: y + 1,
            datos: x,
          });
          throw e;
        } finally {
          client.release();
        }
      })().catch((e) => console.error(e.stack));
    }
  });
});

//TRANSACCION 2
router.get("/transaccion2", function (req, res) {
  pool.query("SELECT * FROM pago", function (err, dat) {
    if (err) {
      console.log(err.stack);
    } else {
      const x = dat.rows.length;
      res.render("transaccion/tran2", {
        ultimoid: x + 1,
        datos: "",
      });
    }
  });
});

router.post("/transaccion2/crear", function (req, res) {
  pool.query("SELECT * FROM pago", function (err, dat) {
    if (err) {
      console.log(err.stack);
    } else {
      const y = dat.rows.length;
      (async () => {
        const client = await pool.connect();
        try {
          datosForm = req.body;
          console.log(datosForm);
          const cs =
            "call tran_pago_causmuerte($1, $2, $3, $4, $5);";
          const csValues = [
            datosForm.id_pago,
            datosForm.id_tipo_pago,
            datosForm.id_tram,
            datosForm.cant_pago,
            datosForm.fch_pago
          ];
          await client.query(cs, csValues);
          x = "Transaccion exitosa";
          res.render("transaccion/tran2", {
            ultimoid: y + 2,
            datos: x,
          });
        } catch (e) {
          x = e;
          res.render("transaccion/tran2", {
            ultimoid: y + 1,
            datos: x,
          });
          throw e;
        } finally {
          client.release();
        }
      })().catch((e) => console.error(e.stack));
    }
  });
});

module.exports = router;
