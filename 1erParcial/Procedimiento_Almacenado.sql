/*==============================================================*/ 
/*                   Procedimiento Almacenado:                  */ 
/*         Ingresar por parametro la causa de muerte,           */ 
/*         y mostrar la cantidad de muertes  y el año .         */ 
/*==============================================================*/  
 CREATE OR REPLACE FUNCTION CANTIDAD_CAUSAMUERTE(text) returns setof "record"
 as $BODY$
 select
   cm.name_cau_mue,
   EXTRACT ( YEAR FROM difunto.fch_muerte_difu) as fecha,
   count (difunto.id_cau_mue)
   FROM difunto 
   join causa_muerte as cm on cm.id_cau_mue = difunto.id_cau_mue
   WHERE cm.name_cau_mue = $1
   group by cm.name_cau_mue, fecha
   $BODY$
   LANGUAGE sql;

  select * from CANTIDAD_CAUSAMUERTE('Covid-19') as 
  ("Causa muerte" text, "Fecha" double precision, "Cantidad" bigint);
