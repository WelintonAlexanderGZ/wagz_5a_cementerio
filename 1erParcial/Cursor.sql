/*==============================================================*/ 
/*            Cursor: Realizar una consulta que permita         */ 
/*                    visualizar los tipos de tramites,         */ 
/*                    la cantidad de pagos y total de pagos.    */ 
/*==============================================================*/
DO $$
DECLARE
    TABLAREGISTRO RECORD;
    CURSOR1 CURSOR FOR
 	SELECT 
		EXTRACT ( YEAR FROM pago.fch_pago) as fecha, 
		tipo_tramite.name_tipo_tram, 
		count (tramite.id_tipo_tram) as cantidad,
		sum (pago.cant_pago) as total
	FROM pago 
		join tramite on tramite.id_tram = pago.id_tram
		join tipo_tramite on tipo_tramite.id_tipo_tram = tramite.id_tipo_tram 
	GROUP BY fecha, tipo_tramite.name_tipo_tram
	ORDER BY fecha ASC;

BEGIN

  FOR TABLAREGISTRO IN CURSOR1 LOOP
  RAISE NOTICE 'Fecha: %, Nombre del tramite: %, Cantidad: %, Total: %',
  TABLAREGISTRO.fecha, TABLAREGISTRO.name_tipo_tram, TABLAREGISTRO.cantidad, TABLAREGISTRO.total;
  END LOOP;

END $$
LANGUAGE 'plpgsql'
