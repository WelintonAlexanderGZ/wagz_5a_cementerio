/*==============================================================*/ 
/*            Consulta 4: Historico de incidentes.              */ 
/*==============================================================*/
SELECT 
	EXTRACT ( YEAR FROM ri.fch_rep_inc) as fecha, 
	ti.name_tipo_inci as tipo_incidente, 
	count (ri.id_tipo_inci) as cantidad_incidente
	FROM reporte_incidente as ri 
	join tipo_incidente as ti on ti.id_tipo_inci = ri.id_tipo_inci
GROUP BY ti.name_tipo_inci, fecha
ORDER BY fecha ASC