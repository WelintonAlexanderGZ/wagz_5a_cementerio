/*==============================================================*/ 
/*            Consulta 2: Historico de pagos.                   */ 
/*==============================================================*/
SELECT 
	EXTRACT ( YEAR FROM pago.fch_pago) as fecha, 
	ti.name_tipo_tram as tipo_tramite, 
	sum (pago.cant_pago) as pago
	FROM pago 
	join tramite as t on t.id_tram = pago.id_tram
	join tipo_tramite as ti on ti.id_tipo_tram = t.id_tipo_tram
GROUP BY ti.name_tipo_tram, fecha
ORDER BY fecha ASC 