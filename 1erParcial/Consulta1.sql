/*==============================================================*/ 
/*            Consulta 1: Historico de certificados.            */ 
/*==============================================================*/
SELECT 
	EXTRACT ( YEAR FROM t.fch_tram) as fecha, 
	ti.name_tipo_tram as tipo_tramite, 
	per.name_per ||' '|| per.lname_per as personal,
	count (ti.name_tipo_tram) as cantidad
	FROM certificado as c 
	join tramite as t on t.id_tram = c.id_tram
	join tipo_tramite as ti on ti.id_tipo_tram = t.id_tipo_tram
	join personal as per on per.id_per = t.id_per 
GROUP BY ti.name_tipo_tram, per.name_per, per.lname_per, EXTRACT ( YEAR FROM t.fch_tram)
ORDER BY EXTRACT ( YEAR FROM t.fch_tram) ASC 
	
	
	