/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     15/11/2021 1:15:18                           */
/*==============================================================*/


/*==============================================================*/
/* Table: CANTON                                                */
/*==============================================================*/
create table CANTON (
   ID_CAN               INT4                 not null,
   NAME_CAN             TEXT                 not null,
   constraint PK_CANTON primary key (ID_CAN)
);

/*==============================================================*/
/* Index: CANTON_PK                                             */
/*==============================================================*/
create unique index CANTON_PK on CANTON (
ID_CAN
);

/*==============================================================*/
/* Table: CAUSA_MUERTE                                          */
/*==============================================================*/
create table CAUSA_MUERTE (
   ID_CAU_MUE           INT4                 not null,
   NAME_CAU_MUE         TEXT                 not null,
   constraint PK_CAUSA_MUERTE primary key (ID_CAU_MUE)
);

/*==============================================================*/
/* Index: CAUSA_MUERTE_PK                                       */
/*==============================================================*/
create unique index CAUSA_MUERTE_PK on CAUSA_MUERTE (
ID_CAU_MUE
);

/*==============================================================*/
/* Table: CERTIFICADO                                           */
/*==============================================================*/
create table CERTIFICADO (
   ID_CERTI             INT4                 not null,
   ID_TRAM              INT4                 not null,
   constraint PK_CERTIFICADO primary key (ID_CERTI)
);

/*==============================================================*/
/* Index: CERTIFICADO_PK                                        */
/*==============================================================*/
create unique index CERTIFICADO_PK on CERTIFICADO (
ID_CERTI
);

/*==============================================================*/
/* Index: TRAMITE_CERTIFICADO_FK                                */
/*==============================================================*/
create  index TRAMITE_CERTIFICADO_FK on CERTIFICADO (
ID_TRAM
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIEN             INT4                 not null,
   ID_UBI               INT4                 not null,
   CI_CLIEN             VARCHAR(10)          not null,
   NAME_CLIEN           TEXT                 not null,
   LNAME_CLIEN          TEXT                 not null,
   FCH_NACI_CLIEN       DATE                 not null,
   TEL1_CLIEN           VARCHAR(10)          not null,
   TEL2_CLIEN           VARCHAR(10)          not null,
   constraint PK_CLIENTE primary key (ID_CLIEN)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIEN
);

/*==============================================================*/
/* Index: UBICACION_CLIENTE_FK                                  */
/*==============================================================*/
create  index UBICACION_CLIENTE_FK on CLIENTE (
ID_UBI
);

/*==============================================================*/
/* Table: DIFUNTO                                               */
/*==============================================================*/
create table DIFUNTO (
   ID_DIFU              INT4                 not null,
   ID_CAU_MUE           INT4                 not null,
   NAME_DIFU            TEXT                 not null,
   LASTN_DIFU           TEXT                 not null,
   FCH_NACI_DIFU        DATE                 not null,
   FCH_MUERTE_DIFU      DATE                 not null,
   CODE_ACTA_DIFU       VARCHAR(5)           not null,
   constraint PK_DIFUNTO primary key (ID_DIFU)
);

/*==============================================================*/
/* Index: DIFUNTO_PK                                            */
/*==============================================================*/
create unique index DIFUNTO_PK on DIFUNTO (
ID_DIFU
);

/*==============================================================*/
/* Index: CAUSAMUERTE_DIFUNTO_FK                                */
/*==============================================================*/
create  index CAUSAMUERTE_DIFUNTO_FK on DIFUNTO (
ID_CAU_MUE
);

/*==============================================================*/
/* Table: ESTADO_ESTRUCTURA                                     */
/*==============================================================*/
create table ESTADO_ESTRUCTURA (
   ID_EST_ESTRUC        INT4                 not null,
   NAME_EST_ESTRUC      TEXT                 not null,
   constraint PK_ESTADO_ESTRUCTURA primary key (ID_EST_ESTRUC)
);

/*==============================================================*/
/* Index: ESTADO_ESTRUCTURA_PK                                  */
/*==============================================================*/
create unique index ESTADO_ESTRUCTURA_PK on ESTADO_ESTRUCTURA (
ID_EST_ESTRUC
);

/*==============================================================*/
/* Table: ESTRUCTURA                                            */
/*==============================================================*/
create table ESTRUCTURA (
   ID_ESTRUC            INT4                 not null,
   ID_LUGAR_ESTRUC      INT4                 not null,
   ID_TIPO_ESTRUC       INT4                 not null,
   ID_EST_ESTRUC        INT4                 not null,
   CRUCES               BOOL                 not null,
   constraint PK_ESTRUCTURA primary key (ID_ESTRUC)
);

/*==============================================================*/
/* Index: ESTRUCTURA_PK                                         */
/*==============================================================*/
create unique index ESTRUCTURA_PK on ESTRUCTURA (
ID_ESTRUC
);

/*==============================================================*/
/* Index: LUGARESTRUCTURA_ESTRUCTURA_FK                         */
/*==============================================================*/
create  index LUGARESTRUCTURA_ESTRUCTURA_FK on ESTRUCTURA (
ID_LUGAR_ESTRUC
);

/*==============================================================*/
/* Index: TIPOESTRUCTURA_ESTRUCTURA_FK                          */
/*==============================================================*/
create  index TIPOESTRUCTURA_ESTRUCTURA_FK on ESTRUCTURA (
ID_TIPO_ESTRUC
);

/*==============================================================*/
/* Index: ESTADOESTRUCTURA_ESTRUCTURA_FK                        */
/*==============================================================*/
create  index ESTADOESTRUCTURA_ESTRUCTURA_FK on ESTRUCTURA (
ID_EST_ESTRUC
);

/*==============================================================*/
/* Table: LUGAR_ESTRUCTURA                                      */
/*==============================================================*/
create table LUGAR_ESTRUCTURA (
   ID_LUGAR_ESTRUC      INT4                 not null,
   NAME_ZONA            TEXT                 not null,
   NAME_MANZA           TEXT                 not null,
   constraint PK_LUGAR_ESTRUCTURA primary key (ID_LUGAR_ESTRUC)
);

/*==============================================================*/
/* Index: LUGAR_ESTRUCTURA_PK                                   */
/*==============================================================*/
create unique index LUGAR_ESTRUCTURA_PK on LUGAR_ESTRUCTURA (
ID_LUGAR_ESTRUC
);

/*==============================================================*/
/* Table: PAGO                                                  */
/*==============================================================*/
create table PAGO (
   ID_PAGO              INT4                 not null,
   ID_TIPO_PAGO         INT4                 not null,
   ID_TRAM              INT4                 not null,
   CANT_PAGO            DECIMAL(6)           not null,
   FCH_PAGO             DATE                 not null,
   constraint PK_PAGO primary key (ID_PAGO)
);

/*==============================================================*/
/* Index: PAGO_PK                                               */
/*==============================================================*/
create unique index PAGO_PK on PAGO (
ID_PAGO
);

/*==============================================================*/
/* Index: TIPOPAGO_PAGO_FK                                      */
/*==============================================================*/
create  index TIPOPAGO_PAGO_FK on PAGO (
ID_TIPO_PAGO
);

/*==============================================================*/
/* Index: TRAMITE_PAGO_FK                                       */
/*==============================================================*/
create  index TRAMITE_PAGO_FK on PAGO (
ID_TRAM
);

/*==============================================================*/
/* Table: PERSONAL                                              */
/*==============================================================*/
create table PERSONAL (
   ID_PER               INT4                 not null,
   ID_TIPO_PER          INT4                 not null,
   CI_PER               VARCHAR(10)          not null,
   NAME_PER             TEXT                 not null,
   LNAME_PER            TEXT                 not null,
   FCH_NACI_PER         DATE                 not null,
   TEL1_PER             VARCHAR(10)          not null,
   TEL2_PER             VARCHAR(10)          not null,
   constraint PK_PERSONAL primary key (ID_PER)
);

/*==============================================================*/
/* Index: PERSONAL_PK                                           */
/*==============================================================*/
create unique index PERSONAL_PK on PERSONAL (
ID_PER
);

/*==============================================================*/
/* Index: TIPOPERSONAL_PERSONAL_FK                              */
/*==============================================================*/
create  index TIPOPERSONAL_PERSONAL_FK on PERSONAL (
ID_TIPO_PER
);

/*==============================================================*/
/* Table: PROVINCIA                                             */
/*==============================================================*/
create table PROVINCIA (
   ID_PRO               INT4                 not null,
   NAME_PRO             TEXT                 not null,
   constraint PK_PROVINCIA primary key (ID_PRO)
);

/*==============================================================*/
/* Index: PROVINCIA_PK                                          */
/*==============================================================*/
create unique index PROVINCIA_PK on PROVINCIA (
ID_PRO
);

/*==============================================================*/
/* Table: REPORTE_INCIDENTE                                     */
/*==============================================================*/
create table REPORTE_INCIDENTE (
   ID_INCI              INT4                 not null,
   ID_TIPO_INCI         INT4                 not null,
   ID_PER               INT4                 not null,
   FCH_REP_INC          DATE                 not null,
   constraint PK_REPORTE_INCIDENTE primary key (ID_INCI)
);

/*==============================================================*/
/* Index: REPORTE_INCIDENTE_PK                                  */
/*==============================================================*/
create unique index REPORTE_INCIDENTE_PK on REPORTE_INCIDENTE (
ID_INCI
);

/*==============================================================*/
/* Index: TIPOINCIDENTE_INCIDENTE_FK                            */
/*==============================================================*/
create  index TIPOINCIDENTE_INCIDENTE_FK on REPORTE_INCIDENTE (
ID_TIPO_INCI
);

/*==============================================================*/
/* Index: PERSONAL_TIPOINCIDENTE_FK                             */
/*==============================================================*/
create  index PERSONAL_TIPOINCIDENTE_FK on REPORTE_INCIDENTE (
ID_PER
);

/*==============================================================*/
/* Table: TIPO_ESTRUCTURA                                       */
/*==============================================================*/
create table TIPO_ESTRUCTURA (
   ID_TIPO_ESTRUC       INT4                 not null,
   NAME_TIPO_ESTRUC     TEXT                 not null,
   constraint PK_TIPO_ESTRUCTURA primary key (ID_TIPO_ESTRUC)
);

/*==============================================================*/
/* Index: TIPO_ESTRUCTURA_PK                                    */
/*==============================================================*/
create unique index TIPO_ESTRUCTURA_PK on TIPO_ESTRUCTURA (
ID_TIPO_ESTRUC
);

/*==============================================================*/
/* Table: TIPO_INCIDENTE                                        */
/*==============================================================*/
create table TIPO_INCIDENTE (
   ID_TIPO_INCI         INT4                 not null,
   NAME_TIPO_INCI       VARCHAR(30)          not null,
   constraint PK_TIPO_INCIDENTE primary key (ID_TIPO_INCI)
);

/*==============================================================*/
/* Index: TIPO_INCIDENTE_PK                                     */
/*==============================================================*/
create unique index TIPO_INCIDENTE_PK on TIPO_INCIDENTE (
ID_TIPO_INCI
);

/*==============================================================*/
/* Table: TIPO_PAGO                                             */
/*==============================================================*/
create table TIPO_PAGO (
   ID_TIPO_PAGO         INT4                 not null,
   NAME_TIPO_PAGO       TEXT                 not null,
   constraint PK_TIPO_PAGO primary key (ID_TIPO_PAGO)
);

/*==============================================================*/
/* Index: TIPO_PAGO_PK                                          */
/*==============================================================*/
create unique index TIPO_PAGO_PK on TIPO_PAGO (
ID_TIPO_PAGO
);

/*==============================================================*/
/* Table: TIPO_PERSONAL                                         */
/*==============================================================*/
create table TIPO_PERSONAL (
   ID_TIPO_PER          INT4                 not null,
   NAME_TIPO_PER        TEXT                 not null,
   constraint PK_TIPO_PERSONAL primary key (ID_TIPO_PER)
);

/*==============================================================*/
/* Index: TIPO_PERSONAL_PK                                      */
/*==============================================================*/
create unique index TIPO_PERSONAL_PK on TIPO_PERSONAL (
ID_TIPO_PER
);

/*==============================================================*/
/* Table: TIPO_TRAMITE                                          */
/*==============================================================*/
create table TIPO_TRAMITE (
   ID_TIPO_TRAM         INT4                 not null,
   NAME_TIPO_TRAM       TEXT                 not null,
   constraint PK_TIPO_TRAMITE primary key (ID_TIPO_TRAM)
);

/*==============================================================*/
/* Index: TIPO_TRAMITE_PK                                       */
/*==============================================================*/
create unique index TIPO_TRAMITE_PK on TIPO_TRAMITE (
ID_TIPO_TRAM
);

/*==============================================================*/
/* Table: TRAMITE                                               */
/*==============================================================*/
create table TRAMITE (
   ID_TRAM              INT4                 not null,
   ID_TIPO_TRAM         INT4                 not null,
   ID_CLIEN             INT4                 not null,
   ID_DIFU              INT4                 not null,
   ID_ESTRUC            INT4                 not null,
   ID_PER               INT4                 not null,
   FCH_TRAM             DATE                 not null,
   FCH_CUMPLI_TRAM      DATE                 not null,
   SMS_TRAM             TEXT                 not null,
   constraint PK_TRAMITE primary key (ID_TRAM)
);

/*==============================================================*/
/* Index: TRAMITE_PK                                            */
/*==============================================================*/
create unique index TRAMITE_PK on TRAMITE (
ID_TRAM
);

/*==============================================================*/
/* Index: TIPOTRAMITE_TRAMITE_FK                                */
/*==============================================================*/
create  index TIPOTRAMITE_TRAMITE_FK on TRAMITE (
ID_TIPO_TRAM
);

/*==============================================================*/
/* Index: CLIENTE_TRAMITE_FK                                    */
/*==============================================================*/
create  index CLIENTE_TRAMITE_FK on TRAMITE (
ID_CLIEN
);

/*==============================================================*/
/* Index: DIFUNTO_TRAMITE_FK                                    */
/*==============================================================*/
create  index DIFUNTO_TRAMITE_FK on TRAMITE (
ID_DIFU
);

/*==============================================================*/
/* Index: ESTRUCTURA_TRAMITE_FK                                 */
/*==============================================================*/
create  index ESTRUCTURA_TRAMITE_FK on TRAMITE (
ID_ESTRUC
);

/*==============================================================*/
/* Index: PERSONAL_TRAMITE_FK                                   */
/*==============================================================*/
create  index PERSONAL_TRAMITE_FK on TRAMITE (
ID_PER
);

/*==============================================================*/
/* Table: UBICACION                                             */
/*==============================================================*/
create table UBICACION (
   ID_UBI               INT4                 not null,
   ID_PRO               INT4                 not null,
   ID_CAN               INT4                 not null,
   constraint PK_UBICACION primary key (ID_UBI)
);

/*==============================================================*/
/* Index: UBICACION_PK                                          */
/*==============================================================*/
create unique index UBICACION_PK on UBICACION (
ID_UBI
);

/*==============================================================*/
/* Index: PROVINCIA_UBICACION_FK                                */
/*==============================================================*/
create  index PROVINCIA_UBICACION_FK on UBICACION (
ID_PRO
);

/*==============================================================*/
/* Index: CANTON_UBICACION_FK                                   */
/*==============================================================*/
create  index CANTON_UBICACION_FK on UBICACION (
ID_CAN
);

alter table CERTIFICADO
   add constraint FK_CERTIFIC_TRAMITE_C_TRAMITE foreign key (ID_TRAM)
      references TRAMITE (ID_TRAM)
      on delete restrict on update restrict;

alter table CLIENTE
   add constraint FK_CLIENTE_UBICACION_UBICACIO foreign key (ID_UBI)
      references UBICACION (ID_UBI)
      on delete restrict on update restrict;

alter table DIFUNTO
   add constraint FK_DIFUNTO_CAUSAMUER_CAUSA_MU foreign key (ID_CAU_MUE)
      references CAUSA_MUERTE (ID_CAU_MUE)
      on delete restrict on update restrict;

alter table ESTRUCTURA
   add constraint FK_ESTRUCTU_ESTADOEST_ESTADO_E foreign key (ID_EST_ESTRUC)
      references ESTADO_ESTRUCTURA (ID_EST_ESTRUC)
      on delete restrict on update restrict;

alter table ESTRUCTURA
   add constraint FK_ESTRUCTU_LUGARESTR_LUGAR_ES foreign key (ID_LUGAR_ESTRUC)
      references LUGAR_ESTRUCTURA (ID_LUGAR_ESTRUC)
      on delete restrict on update restrict;

alter table ESTRUCTURA
   add constraint FK_ESTRUCTU_TIPOESTRU_TIPO_EST foreign key (ID_TIPO_ESTRUC)
      references TIPO_ESTRUCTURA (ID_TIPO_ESTRUC)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_TIPOPAGO__TIPO_PAG foreign key (ID_TIPO_PAGO)
      references TIPO_PAGO (ID_TIPO_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_TRAMITE_P_TRAMITE foreign key (ID_TRAM)
      references TRAMITE (ID_TRAM)
      on delete restrict on update restrict;

alter table PERSONAL
   add constraint FK_PERSONAL_TIPOPERSO_TIPO_PER foreign key (ID_TIPO_PER)
      references TIPO_PERSONAL (ID_TIPO_PER)
      on delete restrict on update restrict;

alter table REPORTE_INCIDENTE
   add constraint FK_REPORTE__PERSONAL__PERSONAL foreign key (ID_PER)
      references PERSONAL (ID_PER)
      on delete restrict on update restrict;

alter table REPORTE_INCIDENTE
   add constraint FK_REPORTE__TIPOINCID_TIPO_INC foreign key (ID_TIPO_INCI)
      references TIPO_INCIDENTE (ID_TIPO_INCI)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_CLIENTE_T_CLIENTE foreign key (ID_CLIEN)
      references CLIENTE (ID_CLIEN)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_DIFUNTO_T_DIFUNTO foreign key (ID_DIFU)
      references DIFUNTO (ID_DIFU)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_ESTRUCTUR_ESTRUCTU foreign key (ID_ESTRUC)
      references ESTRUCTURA (ID_ESTRUC)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_PERSONAL__PERSONAL foreign key (ID_PER)
      references PERSONAL (ID_PER)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_TIPOTRAMI_TIPO_TRA foreign key (ID_TIPO_TRAM)
      references TIPO_TRAMITE (ID_TIPO_TRAM)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_CANTON_UB_CANTON foreign key (ID_CAN)
      references CANTON (ID_CAN)
      on delete restrict on update restrict;

alter table UBICACION
   add constraint FK_UBICACIO_PROVINCIA_PROVINCI foreign key (ID_PRO)
      references PROVINCIA (ID_PRO)
      on delete restrict on update restrict;

