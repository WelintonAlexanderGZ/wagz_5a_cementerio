/*==============================================================*/ 
/*            Consulta 3: Historico de estructuras.             */ 
/*==============================================================*/
	SELECT  
	tie.name_tipo_estruc as tipo_estructura, 
	count (estructura.id_tipo_estruc) as cantidad_estructura
	FROM estructura
	join tipo_estructura as tie on estructura.id_tipo_estruc = tie.id_tipo_estruc
	GROUP BY tie.id_tipo_estruc
	

	