/*==============================================================*/ 
/*            Trigger: Impedir al sepultero realizar            */ 
/*                     mas de 3 tramites el mismo dia.          */ 
/*==============================================================*/
CREATE OR REPLACE FUNCTION RESTRICCION_EMPLEADO() RETURNS TRIGGER
AS 
    $RESTRICCION_EMPLEADO$
DECLARE
    CANTIDAD INT;
BEGIN
    
	SELECT 
		count (tramite.fch_cumpli_tram) into CANTIDAD 
	from tramite
		inner join personal on personal.id_per = tramite.id_per	
	WHERE tramite.id_per = new.id_per and tramite.fch_cumpli_tram = new.fch_cumpli_tram;
    IF(CANTIDAD > 3) THEN
        RAISE EXCEPTION 'El empleado llego al maximo de tramites posibles en esa fecha.';
    END IF;
    RETURN NEW;
END;
$RESTRICCION_EMPLEADO$
LANGUAGE plpgsql;

CREATE TRIGGER RESTRICCION_EMPLEADO AFTER
INSERT ON TRAMITE FOR EACH ROW
EXECUTE PROCEDURE RESTRICCION_EMPLEADO();

INSERT INTO tramite VALUES 
	(13,  2,  9,  9,  13, 4, '2019/12/31', '2020/01/03', 'El trámite se realizará en la fecha y por el personal estipulado.');

INSERT INTO tramite VALUES 
	(14,  2,  7,  7,  11, 4, '2019/12/31', '2020/01/03', 'El trámite se realizará en la fecha y por el personal estipulado.');


	