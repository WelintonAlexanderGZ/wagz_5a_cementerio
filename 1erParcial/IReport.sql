/*==============================================================*/ 
/*    IReport: Total de facellimientos por causas de muerte.    */ 
/*==============================================================*/
select 
   cm.name_cau_mue,
   count (difunto.id_cau_mue)
   FROM difunto 
   join causa_muerte as cm on cm.id_cau_mue = difunto.id_cau_mue
   group by cm.name_cau_mue