/*==============================================================*/
/* Tabla: provincia                                             */
/*==============================================================*/
INSERT INTO public.provincia(
	id_pro, name_pro) VALUES 
	(1, 'Manabí'),
	(2, 'Guayas'),
	(3, 'Esmeraldas');

/*==============================================================*/
/* Tabla: canton                                                */
/*==============================================================*/
INSERT INTO public.canton(
	id_can, name_can) VALUES 
	(1, 'Manta'),
	(2, 'Portoviejo'),
	(3, 'Chone'),
	(4, 'Guayaquil'),
	(5, 'Balzar'),
	(6, 'Colimes'),
	(7, 'Esmeraldas'),
	(8, 'Atacames'),
	(9, 'Quinindé');

/*==============================================================*/
/* Tabla: ubicacion                                             */
/*==============================================================*/
INSERT INTO public.ubicacion(
	id_ubi, id_pro, id_can) VALUES 
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 2, 4),
	(5, 2, 5),
	(6, 2, 6),
	(7, 3, 7),
	(8, 3, 8),
	(9, 3, 9);

/*==============================================================*/
/* Tabla: causa_muerte                                          */
/*==============================================================*/
INSERT INTO public.causa_muerte(
	id_cau_mue, name_cau_mue) VALUES 
	(1, 'Neumonia'),
	(2, 'Covid-19'),
	(3, 'Accidente de transito'),
	(4, 'Diabetes'),
	(5, 'Cancer'),
	(6, 'Homicidio');

/*==============================================================*/
/* Tabla: difunto                                              */
/*==============================================================*/
INSERT INTO public.difunto(
	id_difu, id_cau_mue, name_difu, lastn_difu, fch_naci_difu, fch_muerte_difu, code_acta_difu) VALUES 
	(1, 1, 'Carlos', 'Palma', '1950/11/10', '2019/12/30', 'CAD01'),
	(2, 2, 'Jordy', 'Pico', '1962/08/01', '2020/01/04', 'CAD02'),
	(3, 2, 'Jean', 'Pin', '1940/09/29', '2019/12/30', 'CAD03'),
	(4, 2, 'Manuel', 'Pincay', '1965/04/17', '2020/01/04', 'CAD04'),
	(5, 2, 'Leonardo', 'Quiñonez', '1945/07/16', '2019/12/30', 'CAD05'),
	(6, 3, 'Karen', 'Torres', '1959/12/15', '2020/01/04', 'CAD06'),
	(7, 3, 'Vera', 'Pedro', '1957/11/07', '2019/05/28', 'CAD07'),
	(8, 4, 'Luiggi', 'Zambrano', '1959/12/15', '2020/10/06', 'CAD08'),
	(9, 5, 'Edisson', 'Zambrano', '1980/09/15', '2019/05/28', 'CAD09'),
	(10, 6, 'Branly', 'Yudeh', '1982/09/07', '2020/09/06', 'CAD10'),
	(11, 6, 'Villamar', 'María', '1940/12/20', '2019/05/05', 'CAD11'),
	(12, 6, 'Mera', 'Juan', '1979/10/10', '2020/05/13', 'CAD12');

/*==============================================================*/
/* Tabla: estado_estructura                                     */
/*==============================================================*/
INSERT INTO public.estado_estructura(
	id_est_estruc, name_est_estruc) VALUES 
	(1, 'Disponible'),
	(2, 'Ocupado'),
	(3, 'En construcción'),
	(4, 'En mantenimiento');

/*==============================================================*/
/* Tabla: tipo_estructura                                       */
/*==============================================================*/
INSERT INTO public.tipo_estructura(
	id_tipo_estruc, name_tipo_estruc) VALUES 
	(1, 'Edificaciones del Personal'),
	(2, 'Panteon'),
	(3, 'Tumba'),
	(4, 'Nicho'),
	(5, 'Columbario');

/*==============================================================*/
/* Tabla: lugar_estructura                                      */
/*==============================================================*/
INSERT INTO public.lugar_estructura(
	id_lugar_estruc, name_zona, name_manza)VALUES 
	(1, 'Zona 1', 'Manzana 1'),
	(2, 'Zona 1', 'Manzana 2'),
	(3, 'Zona 1', 'Manzana 3'),
	(4, 'Zona 2', 'Manzana 1'),
	(5, 'Zona 2', 'Manzana 2'),
	(6, 'Zona 3', 'Manzana 1'),
	(7, 'Zona 3', 'Manzana 2'),
	(8, 'Zona 4', 'Manzana 1'),
	(9, 'Zona 4', 'Manzana 2'),
	(10, 'Zona 5', 'Manzana 1'),
	(11, 'Zona 5', 'Manzana 2');

/*==============================================================*/
/* Tabla: estructura                                            */
/*==============================================================*/
INSERT INTO public.estructura(
	id_estruc, id_lugar_estruc, id_tipo_estruc, id_est_estruc, cruces)VALUES 
	(1, 1, 1, 1, false),
	(2, 2, 1, 1, false),
	(3, 3, 1, 3, false),	
	(4, 4, 2, 2, true),
	(5, 5, 2, 3, false),
	(6,  6, 3, 2, true),
	(7,  6, 3, 2, true),
	(8,  6, 3, 2, true),
	(9,  6, 3, 1, true),
	(10, 7, 3, 1, true),
	(11, 7, 3, 1, true),
	(12, 7, 3, 1, true),
	(13, 7, 3, 2, true),	
	(14, 8, 4, 2, true),
	(15, 8, 4, 1, true),
	(16, 9, 4, 1, true),
	(17, 9, 4, 1, true),	
	(18, 10, 5, 1, true),
	(19, 11, 5, 3, false);

/*==============================================================*/
/* Tabla: cliente                                               */
/*==============================================================*/
INSERT INTO public.cliente(
	id_clien, id_ubi, ci_clien, name_clien, lname_clien, fch_naci_clien, tel1_clien, tel2_clien)VALUES 
	(1  , 1, '1321545845', 'Eber', 'Alarcon', '1998/11/10', '0997545121', '0997545121'),
    (2  , 1, '1354545144', 'Jose', 'Arteaga', '1990/12/11', '0986537212', '0986537212'),
	(3  , 1, '1351545411', 'Juan', 'Cedeño', '1995/01/17', '0995845411', '0995845411'),
	(4  , 1, '1315454588', 'Dalember', 'Gorozabel', '1990/11/01', '0988745112', '0988745112'),
	(5  , 2, '1312545412', 'Anthony', 'Holguin', '1980/05/09', '0947878511', '0947878511'),
	(6  , 3, '1354547899', 'Helen', 'Lucas', '1992/06/26', '0984513265', '0984513265'),
	(7  , 4, '1312356548', 'Nahomi', 'Machuca', '1985/07/23', '0988751451', '0988751451'),
	(8  , 5, '1364895987', 'Brando', 'Mero', '1998/06/07', '0978451210', '0978451210'),
	(9  , 6, '1315447123', 'Kelvin', 'Muñoz', '1997/04/04', '0988451326', '0988451326'),
	(10 , 7, '1314558866', 'Luis', 'Navarrete', '1988/06/06', '0947845123', '0947845123'),
	(11 , 8, '1354665847', 'Antony', 'Palacios', '1979/08/14', '0915454758', '0915454758'),
	(12 , 9, '1312454763', 'Kevin', 'Ponce', '1995/10/22', '0988754512', '0988754512');

/*==============================================================*/
/* Tabla: tipo_personal                                         */
/*==============================================================*/
INSERT INTO public.tipo_personal(
	id_tipo_per, name_tipo_per) VALUES 
	(1, 'Administrador'),
	(2, 'Sepulturero'),
	(3, 'Encargado de limpieza'),
	(4, 'Guardia de seguridad');

/*==============================================================*/
/* Tabla: personal                                              */
/*==============================================================*/
INSERT INTO public.personal(
	id_per, id_tipo_per, ci_per, name_per, lname_per, fch_naci_per, tel1_per, tel2_per)VALUES 
	(1, 1, '1305178051', 'Welinton', 'Guerrero', '1998/12/22', '0989194770', '0989194770'),
	(2, 1, '1315250025', 'Cristhoper', 'Alcivar', '1998/12/22', '0997480125', '0997480125'),
	(3, 1, '1301456874', 'Axel', 'Arteaga', '2000/02/15', '0987845163', '0987845163'),	
	(4, 2, '1315484778', 'Benjie', 'Gonzalez', '1998/05/30', '0984578451', '0984578451'),
	(5, 2, '1305465454', 'Sergio', 'Lino', '1995/01/01', '0996558458', '0996558458'),
	(6, 2, '1347889941', 'Julexi', 'Marquez', '1998/03/25', '0993211456', '0993211456'),	
	(7, 3, '1305458781', 'Isaac', 'Cedeño', '1998/05/30', '0987451657', '0987451657'),
	(8, 3, '1354445441', 'Joffre', 'Rodriguez', '2000/12/02', '0987451211', '0987451211'),
	(9, 3, '1396654712', 'Erick', 'Carreño', '1998/05/30', '0987455120', '0987455120'),	
	(10 , 4, '1315454781', 'Gustavo', 'Rodriguez', '1998/05/30', '0994551121', '0975451121'),
	(11 , 4, '1305451451', 'Jose', 'Reyes', '1999/12/10', '0987451211', '0987451211'),
	(12 , 4, '1306454411', 'Erick', 'Carreño', '1998/05/30', '0987455120', '0987455120');

/*==============================================================*/
/* Tabla: tipo_incidente                                        */
/*==============================================================*/
INSERT INTO public.tipo_incidente(
	id_tipo_inci, name_tipo_inci)VALUES 
	(1, 'Robo de feretros'),
	(2, 'Actos vandalicos'),
	(3, 'Uso de sustancias psicoactivas'),
	(4, 'Acto sexual');
	
/*==============================================================*/
/* Table: reporte_incidente                                     */
/*==============================================================*/
INSERT INTO public.reporte_incidente(
	id_inci, id_tipo_inci, id_per, fch_rep_inc)VALUES 
	(1, 1, 10, '2019/04/05'),
	(2, 1, 10, '2020/12/09'),
	(3, 2, 10, '2019/02/11'),
	(4, 3, 10, '2020/07/23'),
	(5, 2, 11, '2019/08/04'),
	(6, 3, 11, '2020/09/16'),
	(7, 4, 11, '2019/10/30'),
	(8, 4, 11, '2020/01/28'),	
	(9  , 1, 12, '2019/05/13'),
	(10 , 2, 12, '2020/09/08'),
	(11 , 3, 12, '2019/11/03'),
	(12 , 4, 12, '2020/11/12');

/*==============================================================*/
/* Table: tipo_tramite                                          */
/*==============================================================*/
INSERT INTO public.tipo_tramite(
	id_tipo_tram, name_tipo_tram) VALUES 
	(1, 'Inhumación'),
	(2, 'Exhumación');

/*==============================================================*/
/* Tabla: tipo_pago                                             */
/*==============================================================*/
INSERT INTO public.tipo_pago(
	id_tipo_pago, name_tipo_pago)VALUES 
	(1, 'Debito bancario'),
	(2, 'Pago con efectivo en ventanilla'),
	(3, 'Pago con tarjeta de credito en ventanilla');

/*==============================================================*/
/* Table: tramite                                               */
/*==============================================================*/
INSERT INTO public.tramite(
	id_tram, id_tipo_tram, id_clien, id_difu, id_estruc, id_per, fch_tram, fch_cumpli_tram, sms_tram) VALUES 
	(1,  1,  1,  1,  4, 4, '2019/12/31', '2020/01/03', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(2,  1,  2,  2,  6, 4, '2020/01/05', '2020/01/08', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(3,  1,  3,  3,  7, 4, '2019/12/31', '2020/01/03', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(4,  1,  4,  4,  8, 4, '2020/01/05', '2020/01/08', 'El trámite se realizará en la fecha y por el personal estipulado.'),	
	(5,  2,  5,  5,  9, 5, '2019/12/31', '2020/01/03', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(6,  2,  6,  6, 10, 5, '2020/01/05', '2020/01/08', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(7,  1,  7,  7, 11, 5, '2019/05/29', '2019/06/02', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(8,  2,  8,  8, 12, 5, '2020/10/07', '2020/10/10', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(9 , 1,  9,  9, 13, 6, '2019/05/29', '2019/06/02', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(10, 1, 10, 10, 14, 6, '2020/09/07', '2020/09/10', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(11, 2, 11, 11, 16, 6, '2019/05/06', '2019/05/09', 'El trámite se realizará en la fecha y por el personal estipulado.'),
	(12, 2, 12, 12, 18, 6, '2020/05/14', '2020/05/17', 'El trámite se realizará en la fecha y por el personal estipulado.');

/*==============================================================*/
/* Table: certificado                                           */
/*==============================================================*/
INSERT INTO public.certificado(
	id_certi, id_tram)VALUES 
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7),
	(8, 8),
	(9, 9),
	(10, 10),
	(11, 11),
	(12, 12);

/*==============================================================*/
/* Tabla: pago                                                  */
/*==============================================================*/
alter table pago alter column cant_pago set data type decimal; 
INSERT INTO public.pago(
	id_pago, id_tipo_pago, id_tram, cant_pago, fch_pago)VALUES 
	(1, 1, 1, 5000.75, '2019/12/31'),
	(2, 2, 2, 3000.15, '2020/01/05'),
	(3, 3, 3, 3000.15, '2019/12/31'),
	(4, 1, 4, 3000.15, '2020/01/05'),
	(5, 2, 5, 3500.25, '2019/12/31'),
	(6, 3, 6, 3500.25, '2020/01/05'),
	(7, 1, 7, 3500.25, '2019/05/29'),
	(8, 2, 8, 3500.25, '2020/10/07'),
	(9, 3, 9, 3000.15, '2019/05/29'),
	(10, 1, 10, 4000.75, '2020/09/07'),
	(11, 2, 11, 4500.15, '2019/05/06'),
	(12, 3, 12, 2500.25, '2020/05/14');


